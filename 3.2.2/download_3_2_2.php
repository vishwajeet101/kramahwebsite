<?php 
header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=3_2_2.csv');

// create a file pointer connected to the output stream
$output = fopen('php://output', 'w');

// output the column headings
fputcsv($output, array('Upload documents','file_image', 'Name_of_the_Project_Endowments_Chairs', 'Name_of_the_Principal_Investigator_Co_Investigator', 'Department_of_Principal_Investigator_Co_Investigator', 'Year_of_award', 'Funds_provided', 'Duration_of_the_project', 'Year_introduction'));

 error_reporting(E_ALL);

include('../dbconfigure.php');
// fetch the data

$rows = mysqli_query($con,'SELECT file_image, Name_of_the_Project_Endowments_Chairs, Name_of_the_Principal_Investigator_Co_Investigator, Department_of_Principal_Investigator_Co_Investigator, Year_of_award, Funds_provided, Duration_of_the_project, Year_introduction FROM metricsn3_2_2');

// loop over the rows, outputting them
while ($row = mysqli_fetch_assoc($rows)) fputcsv($output, $row);
?>
