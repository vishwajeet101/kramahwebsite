<?php
//load the database configuration file
include 'd_3_2_2.php';

if(isset($_POST['importSubmit'])){
    
    //validate whether uploaded file is a csv file
    $csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
    if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'],$csvMimes)){
        if(is_uploaded_file($_FILES['file']['tmp_name'])){
            
            //open uploaded csv file with read only mode
            $csvFile = fopen($_FILES['file']['tmp_name'], 'r');
            
            //skip first line
            fgetcsv($csvFile);
            
            //parse data from csv file line by line
            while(($line = fgetcsv($csvFile)) !== FALSE){
                //check whether member already exists in database with same email
                $prevQuery = "SELECT id FROM metricsn3_2_2 WHERE file_image= '".$line[0]."'";
                $prevResult = $con->query($prevQuery);
                if($prevResult->num_rows > 0){
                    //update member data
                    $con->query("UPDATE metricsn3_2_2 SET 
		Name_of_the_Project_Endowments_Chairs= '".$line[1]."',
		Name_of_the_Principal_Investigator_Co_Investigator= '".$line[2]."',
		Department_of_Principal_Investigator_Co_Investigator= '".$line[3]."',
		Year_of_award= '".$line[4]."',
    Funds_provided= '".$line[5]."',
    Duration_of_the_project= '".$line[6]."',
    Year_introduction = '".$line[7]."' where file_image = '".$line[0]."'");
                }else{
                    //insert member data into database
                    $con->query("INSERT INTO metricsn3_2_2(file_image, Name_of_the_Project_Endowments_Chairs, Name_of_the_Principal_Investigator_Co_Investigator, Department_of_Principal_Investigator_Co_Investigator, Year_of_award, Funds_provided, Duration_of_the_project, Year_introduction)
	VALUES('".$line[0]."','".$line[1]."','".$line[2]."','".$line[3]."','".$line[4]."','".$line[5]."','".$line[6]."','".$line[7]."')");
            
    }
            }
            
            //close opened csv file
           mysql_query($con->query);
  ?>
  <script>
  alert('successfully uploaded');
        window.location.href='home_3_2_2.php?success';
        </script>
  <?php
        }else
 {
  ?>
  <script>
  alert('error while uploading file');
        window.location.href='home_3_2_2.php?fail';
        </script>
  <?php
 }
    }
}

//redirect to the listing page
header("Location: home_3_2_2.php".$qstring);