<?php 
header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=3_4_6.csv');

// create a file pointer connected to the output stream
$output = fopen('php://output', 'w');

// output the column headings
fputcsv($output, array('SL_No','Name_of_the_teacher','Title_of_the_book','Title_of_paper','Title_of_the_proceedings_of_conference','Name_of_the_conference','National_International','Year_of_publication','ISBN_ISSN_Number_of_proceeding','Affiliating_Institute_at_the_time_of_publication','Name_of_the_publisher'));

 error_reporting(E_ALL);

include('../dbconfigure.php');
// fetch the data

$rows = mysqli_query($con,'SELECT SL_No,Name_of_the_teacher,Title_of_the_book,Title_of_paper,Title_of_the_proceedings_of_conference,Name_of_the_conference,National_International,Year_of_publication,ISBN_ISSN_Number_of_proceeding,Affiliating_Institute_at_the_time_of_publication,Name_of_the_publisher FROM metrics_3_4_6');

// loop over the rows, outputting them
while ($row = mysqli_fetch_assoc($rows)) fputcsv($output, $row);
?>
