<?php
//load the database configuration file
include 'd_3_4_6.php';

if(isset($_POST['importSubmit'])){

    //validate whether uploaded file is a csv file
  $csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
  if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'],$csvMimes)){
    if(is_uploaded_file($_FILES['file']['tmp_name'])){

            //open uploaded csv file with read only mode
      $csvFile = fopen($_FILES['file']['tmp_name'], 'r');

            //skip first line
      fgetcsv($csvFile);

            //parse data from csv file line by line
      while(($line = fgetcsv($csvFile)) !== FALSE){
                //check whether member already exists in database with same email
  //               $prevQuery = "SELECT id FROM metrics_3_4_6 WHERE fileImage= '".$line[0]."'";
  //               $prevResult = $con->query($prevQuery);
  //               if($prevResult->num_rows > 0){
  //                   //update member data
  //                   $con->query("UPDATE metrics_3_4_6 SET 
		// SL_No= '".$line[1]."',
		// Name_of_the_teacher= '".$line[2]."',
		// Title_of_the_book= '".$line[3]."',
  //   Title_of_paper= '".$line[4]."',
  //   Title_of_the_proceedings_of_conference= '".$line[5]."',
  //   Name_of_the_conference= '".$line[6]."',
  //   National_International= '".$line[7]."',
  //   Year_of_publication= '".$line[8]."',
		// ISBN_ISSN_Number_of_proceeding= '".$line[9]."',
  //   Affiliating_Institute_at_the_time_of_publication= '".$line[10]."',
		// Name_of_the_publisher = '".$line[11]."' where fileImage = '".$line[0]."'");
  //               }else{
                    //insert member data into database
        $con->query("INSERT INTO metrics_3_4_6(SL_No,Name_of_the_teacher,Title_of_the_book,Title_of_paper,Title_of_the_proceedings_of_conference,Name_of_the_conference,National_International,Year_of_publication,ISBN_ISSN_Number_of_proceeding,Affiliating_Institute_at_the_time_of_publication,Name_of_the_publisher)
         VALUES('".$line[0]."','".$line[1]."','".$line[2]."','".$line[3]."','".$line[4]."','".$line[5]."','".$line[6]."','".$line[7]."','".$line[8]."','".$line[9]."','".$line[10]."')");

    // }
      }

            //close opened csv file
      mysql_query($con->query);
      ?>
      <script>
        alert('successfully uploaded');
        window.location.href='home_3_4_6.php?success';
      </script>
      <?php
    }else
    {
      ?>
      <script>
        alert('error while uploading file');
        window.location.href='home_3_4_6.php?fail';
      </script>
      <?php
    }
  }
}

//redirect to the listing page
header("Location: home_3_4_6.php".$qstring);